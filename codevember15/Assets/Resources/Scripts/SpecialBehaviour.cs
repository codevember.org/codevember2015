﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialBehaviour : MonoBehaviour {

	public GameLogic gameLogic; 
	public string type;
	private int lifetime = 0;
	private bool activated = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.localScale *= 1.01f;
		lifetime++;

		if (lifetime > 80 && !activated && type == "slowmotion")
			Destroy (gameObject);

		if (lifetime > 50 && !activated && type == "boom")
			Destroy (gameObject);
	}

	private IEnumerator EndSlowMotion() {
		yield return new WaitForSeconds (1.0f);

		gameLogic.resetTime ();
		gameLogic.gameObject.GetComponent<AudioSource>().pitch *= 2.0f;

		Destroy (gameObject);
	}
		
	public void Clicked () {
		this.activated = true;

		switch (this.type) {
		case "slowmotion":
			if (gameLogic.slowMotion == true)
				Debug.Log ("Getting deeper into the rabbit hole");
			
			gameLogic.slowMotion = true;
			Time.timeScale /= 3.0f;
			gameLogic.gameObject.GetComponent<AudioSource>().pitch *= 0.5f;
			StartCoroutine (EndSlowMotion());
			break;

		case "boom":
			gameLogic.triggerGameOver ();
			//Debug.Log ("you shouldn't have done this");
			break;
		}

		// because disable doesn't work (coroutine), just hide the text
		//this.gameObject.GetComponent<Renderer> ().enabled = false;
		//this.gameObject.SetActive(false);
		this.GetComponent<Text>().text = "";
		gameObject.transform.localScale *= 0.0f;
	}
}