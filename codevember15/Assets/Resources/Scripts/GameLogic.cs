﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour {
	
	public AudioClip loop;
	public GameObject canvas_screen;
	public ParticleSystem particles;
	public GameObject circles_container;
	public GameObject scoring_field;
	public GameObject menu_music;

	public GameObject specials_container;
	public bool slowMotion = false;

	bool loop_is_running = false;
	bool is_running = false;
	bool gameover = false;

	public int framecount;
	int circlecount;
	int circlecount_achievements;
	int spawn_speed;
	int next_spawn;

	GameObject repeator;
	GameObject temp;
	Queue<GameObject> circles = new Queue<GameObject>();

	Vector3 newPos;
	Vector3 latestHit;
	GameObject circlePrefab;
	GameObject circleWhitePrefab;
	GameObject slowmotionPrefab;
	GameObject boomPrefab;

	private int start_circle_size;
	private int max_circle_lifetime;
	private Achievements achievement_skript;
	private int next_special = 500;


	void Start() {
		achievement_skript = gameObject.GetComponent<Achievements> ();
		if(GameObject.Find("repeator") == null) {
			repeator = new GameObject();
			repeator.name = "repeator";
			DontDestroyOnLoad(repeator);
			repeator.AddComponent<repeatController>();
		}
		if(GameObject.Find("repeator") != null) 
			repeator = GameObject.Find("repeator");
		if(repeator.GetComponent<repeatController>().new_round)
			gameObject.GetComponent<StartGame>().Run(false);

		// get special prefab
		slowmotionPrefab = Resources.Load("Prefabs/Special-Slow", typeof(GameObject)) as GameObject;
		(slowmotionPrefab.GetComponent<SpecialBehaviour> () as SpecialBehaviour).gameLogic = this;
		boomPrefab = Resources.Load("Prefabs/Special-Boom", typeof(GameObject)) as GameObject;
		(boomPrefab.GetComponent<SpecialBehaviour> () as SpecialBehaviour).gameLogic = this;

		// Get prefab of spawning circle (image + text)
		circlePrefab = Resources.Load("Prefabs/Circle", typeof(GameObject)) as GameObject;
		circleWhitePrefab = Resources.Load("Prefabs/Circle-White", typeof(GameObject)) as GameObject;
		Text[] texts = {
			this.circlePrefab.transform.Find ("Text").GetComponent<Text> (),
			this.circleWhitePrefab.transform.Find ("Text").GetComponent<Text> ()
		};

		// determin values with threshold for tablet and phone
		if ((Screen.height / Screen.dpi) < 3.1f) {
			// it's a phone
			// FullHD Displays
			if (Screen.width >= 1920 && Screen.height >= 1080) {
				// set collider size of circle (radius)!!!
				this.start_circle_size = 60;
				// update text size of circle
				foreach  (Text t in texts) {
					t.resizeTextMinSize = 14;
					t.resizeTextMaxSize = 50;
				}
			} else {
				// All other Displays
				// set collider size of circle (radius)!!!
				this.start_circle_size = 55;
				// update text size of circle
				foreach (Text t in texts) {
					t.resizeTextMinSize = 14;
					t.resizeTextMaxSize = 45;
				}
			}
		} else {
			// it's a tablet
			// set collider size of circle (radius)!!!
			this.start_circle_size = 50;
			// update text size of circle
			foreach  (Text t in texts) {
				t.resizeTextMinSize = 14;
				t.resizeTextMaxSize = 40;
			}
		}

		this.max_circle_lifetime = 2 * this.start_circle_size;	

		// set image size of circle!!!
		circlePrefab.GetComponent<RectTransform> ().sizeDelta = new Vector2 (max_circle_lifetime, max_circle_lifetime);

		// init params
		framecount = 0;
		circlecount = 1;
		spawn_speed = 70;
		next_spawn = 70;
		// get circle count for unlocking achievements
		circlecount_achievements = PlayerPrefs.GetInt ("Circlecount");
	}

	// play some music
	IEnumerator GameMusicStart() {
		menu_music.GetComponent<AudioSource>().Stop();
		AudioSource audio = GetComponent<AudioSource>();
		audio.Play();
		yield return new WaitForSeconds(audio.clip.length - 0.45f);
		if (!gameover) {
			audio.clip = loop;
			audio.Play ();
		}
		// update temp
		loop_is_running = true;
	}

	public IEnumerator plopAnim(Transform t, Color c) {
		//  no CLONE particles but it isnt possible now to play two particles at the same time
		if (particles.isPlaying)
			particles.Stop ();
		particles.Clear ();
		particles.transform.position = t.position;
		particles.startColor = c;
		particles.Play ();
		yield return null;
	}

	public void setRunning() {
		is_running = true;
		gameObject.AddComponent<Score>();
		circles_container.SetActive(true);
		StartCoroutine(GameMusicStart());
	}

	IEnumerator WaitForMusic() {
		yield return new WaitForSeconds(0.5f);
		gameover = true;
		menu_music.GetComponent<AudioSource> ().Play ();
	}

	public void unsetRunning() {
		is_running = false;
		AudioSource audio = GetComponent<AudioSource>();
		audio.Stop();
		//wait for Popped Sound to finish .. .so delay Menu Music
		StartCoroutine (WaitForMusic ());
	}

	public Queue<GameObject> getCircles () {
		return circles;
	}

	// Update is called once per frame
	void Update () {
		// run game logic
		if(is_running) {
			framecount++;

			// spawn slow action
			if (--this.next_special == 0)
				handleSpecialSpawn ();
			
			// create a new circle
			if(next_spawn == 0) {
				handleCircleSpawn();
				next_spawn = spawn_speed;
			}

			// respect slow motion
			if (slowMotion) {
				if (framecount % 3 == 0)
					next_spawn--;
			}
			else 
				next_spawn--;
			
			// Mouse input (left mouse button)
			if (Input.GetMouseButtonDown (0))
				checkHitBox (Physics2D.Raycast(Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero));
			// Touch input for mobile devices
			if (Input.touchCount == 1)
				checkHitBox (Physics2D.Raycast(Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position), Vector2.zero));
			//Unlock Achievement when touching with more then one finger
			if (Input.touchCount > 1) 
				achievement_skript.UnlockAchievement("Multi Fingering");
			// increase difficulty
			if(framecount % 60 == 0 && spawn_speed > 30)
				spawn_speed--;
		}
	}

	private void checkHitBox(RaycastHit2D hit) {
		// no hit on a collider
		if (hit.collider == null)
			return;
		// circle was hit
		if (circles.Dequeue ().name.Equals (hit.transform.gameObject.name)) {
			// start particle animation if circle gets destroyed
			StartCoroutine(plopAnim(hit.transform, new Color (1, 1, 1)));
			// update score
			gameObject.GetComponent<Score>().updateScore(1);
			// update latest hit position
			latestHit = hit.transform.gameObject.GetComponent<CirclesBehaviour>().getPosition;
			// delete object
			Destroy (hit.transform.gameObject);
			// play hit sound
			Camera.main.GetComponent<AudioSource> ().Play();
			next_spawn = 0;
		} else {
			// start colored particle animation because of wrong clicked circle
			StartCoroutine(plopAnim(hit.transform, new Color (1, 1, 1)));
			// update amount of clicked circles
			PlayerPrefs.SetInt("Circlecount", circlecount_achievements);
			// Achievement
			achievement_skript.UnlockAchievement("Fuck The System");

			triggerGameOver ();
		}
	}

	public void triggerGameOver () {
		// set game over
		gameObject.GetComponent<Gameover> ().PopupGameover();
		this.resetTime ();
		Transform[] childs = circles_container.GetComponentsInChildren<Transform> ();
		foreach (Transform c in childs) {
			Destroy (c.gameObject);
			//Debug.Log (sh);
		}
	}

	public void handleSpecialSpawn() {

		RectTransform canvas_transform = canvas_screen.GetComponent<RectTransform> ();
		float width = canvas_transform.rect.width;
		float height = canvas_transform.rect.height;
		bool overlay = true;

		// get height of scoreboard
		float score_height = canvas_screen.GetComponentInChildren<Score> ().GetComponent<RectTransform> ().rect.height;

		while(overlay) {
			float x = Random.Range (-(width / 2), (width / 2));
			// we substract the score high so no circle can spawn behind or in front of the scoreboard
			float y = Random.Range (-(height / 2), (height / 2) - score_height);
			newPos = new Vector3 (x, y, 0);
			// manage more than one circle
			if (circles.Count >= 0) {
				bool isOver = true;
				if (Mathf.Sqrt ((newPos - latestHit).sqrMagnitude) <= max_circle_lifetime * 2) {
					isOver = false;
				} else {
					foreach (GameObject c in circles) {
						if (Mathf.Sqrt ((newPos - c.GetComponent<CirclesBehaviour>().getPosition).sqrMagnitude) <= max_circle_lifetime * 2) {
							isOver = false;
							break;
						}
					}
				}
				if (isOver)
					break;
				else
					continue;
			}
		}
		newPos.x += width / 2;
		newPos.y += height / 2; 

		// randomize the special buttons
		GameObject temp = null;
		if (Random.Range (0.0f, 1.0f) <= 0.7f) {
			temp = Instantiate (slowmotionPrefab);
			temp.GetComponent<SpecialBehaviour> ().type = "slowmotion";
		} else {
			temp = Instantiate (boomPrefab);
			temp.GetComponent<SpecialBehaviour> ().type = "boom";
		}

		temp.transform.SetParent(specials_container.transform);
		temp.transform.position = newPos;

		// random rotation
		temp.transform.Rotate(Vector3.forward * Random.Range(-10,10));

		// set the next spawn
		this.next_special = (int)Random.Range (100.0f, 1200.0f);

		// this seems to happen never :D
		// deeper the rabbit hole
		if (this.next_special < 200.0f)
			this.next_special = 50;
	}

	public void handleCircleSpawn() {

		RectTransform canvas_transform = canvas_screen.GetComponent<RectTransform> ();
		float width = canvas_transform.rect.width;
		float height = canvas_transform.rect.height;
		bool overlay = true;

		// get height of scoreboard
		float score_height = canvas_screen.GetComponentInChildren<Score> ().GetComponent<RectTransform> ().rect.height;

		while(overlay) {
			float x = Random.Range (-(width / 2), (width / 2));
			// we substract the score high so no circle can spawn behind or in front of the scoreboard
			float y = Random.Range (-(height / 2), (height / 2) - score_height);
			newPos = new Vector3 (x, y, 0);
			// manage more than one circle
			if (circles.Count >= 0) {
				bool isOver = true;
				if (Mathf.Sqrt ((newPos - latestHit).sqrMagnitude) <= max_circle_lifetime * 2) {
					isOver = false;
				} else {
					foreach (GameObject c in circles) {
						if (Mathf.Sqrt ((newPos - c.GetComponent<CirclesBehaviour>().getPosition).sqrMagnitude) <= max_circle_lifetime * 2) {
							isOver = false;
							break;
						}
					}
				}
				if (isOver)
					break;
				else
					continue;
			}
		}
		// spawn new circle with computed position
		spawnCircle (newPos, this.start_circle_size, max_circle_lifetime);
		
		// update sound pitch
		updateSoundPitch ();
	}

	private void spawnCircle(Vector3 pos, int start_c_size, int max_lifetime) {
		GameObject temp = null;
		if (circlecount % 10 != 0)
			temp = Instantiate(circlePrefab);
		else
			temp = Instantiate(circleWhitePrefab);

		temp.transform.SetParent(circles_container.transform);
		temp.name = circlecount.ToString();
		temp.GetComponent<CirclesBehaviour> ().SpawnCircle (newPos, start_c_size, max_lifetime, circlecount);
		circles.Enqueue(temp);
		// update number in circles
		++circlecount;
		// update amount of destroyed circles
		++circlecount_achievements;
		// check if achievement gets unlocked
		checkAmountOfClickedCircles (circlecount_achievements);
	}

	private void checkAmountOfClickedCircles(int circlecount_achievements) {
		//Achievements
		if (circlecount_achievements == 50) 
			achievement_skript.UnlockAchievement("Bicircle");
		if (circlecount_achievements == 250)
			achievement_skript.UnlockAchievement("What Goes Around");
		if (circlecount_achievements == 500) 
			achievement_skript.UnlockAchievement("Comes Back Around");
		if (circlecount_achievements == 1000) 
			achievement_skript.UnlockAchievement("They See Me Rollin");
		if (circlecount_achievements == 5000) 
			achievement_skript.UnlockAchievement("Finger Licking Good");
		if (circlecount_achievements == 10000) 
			achievement_skript.UnlockAchievement("Che Guefinger");
		if (circlecount_achievements == 50000) 
			achievement_skript.UnlockAchievement("Kreiselficker");
	}

	private void updateSoundPitch() {
		if (loop_is_running && circlecount % 5 == 0) {
			gameObject.GetComponent<AudioSource>().pitch += 0.01f;
		}
	}

	public void exitGame() {
		// save amount of clicked circles
		PlayerPrefs.SetInt ("Circlecount", circlecount_achievements);
		// exit application
		Application.Quit();
	}

	public void resetTime() {
		if (slowMotion) {
			slowMotion = false;
			Time.timeScale = 1.0f;
		}
	}
}