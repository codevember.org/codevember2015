﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CirclesBehaviour : MonoBehaviour {

	// set game manager for particle effects and game over status
	public GameObject gameManger;
	private GameLogic gameLogic;

	private int circle_number;
	private int lifetime;
	private int max_lifetime;
	private int start_size;
	RectTransform circle_transform;
	CircleCollider2D circle_collider;
	Vector3 position;

	public float getStartSize { get { return start_size; } }
	public float getMaxLifeTime { get { return max_lifetime; } }
	public float getCurLifetime { get { return lifetime; } }
	public Vector3 getPosition { get { return position; } }

	public void SpawnCircle (Vector3 pos, int start_size, int max_lifetime, int circle_number) {

		// init start parameters of circle
		this.max_lifetime = max_lifetime;
		this.start_size = start_size;
		this.position = pos;
		this.circle_number = circle_number;
		this.lifetime = 0;

		circle_transform = gameObject.GetComponent<RectTransform>();
		circle_transform.Translate(pos, Space.World);

		// random rotation
		circle_transform.Rotate(Vector3.forward * Random.Range(-10,10));
		// let every 10th circle be bigger
		if((circle_number % 10) == 0)
			circle_transform.localScale *= 1.50f;

		gameObject.AddComponent<CircleCollider2D>();
		circle_collider = gameObject.GetComponent<CircleCollider2D>();
		circle_collider.radius = start_size;

		// set number of circle
		setNumber(circle_number);
	}

	// set the number of the spawning circles
	private void setNumber(int number) {

		gameObject.name = number.ToString();
		
		foreach (Transform child in transform) {
			if (child.name == "Text")
				child.GetComponent<Text>().text = number.ToString();
		}
	}

	void Start() {
		gameManger = GameObject.Find ("GameManager");
		gameLogic = gameManger.GetComponent<GameLogic> () as GameLogic;
	}

	void Update() {
		// let circle grow
		gameObject.transform.localScale *= 1.01f;

		// update lifetime with respect slow motion
		if (gameLogic.slowMotion) {
			if (gameLogic.framecount % 3 == 0)
				lifetime++;
		}
		else 
			lifetime++;
		
		// update lifetime// check current size of circle and set game over if circle is too big
		if (lifetime > max_lifetime) {
			// start red particle animation if circle plops
			StartCoroutine (gameManger.GetComponent<GameLogic> ().plopAnim (this.transform, new Color(1,1,1)));
			// unlock achievement "The NewB" if no circle was hit
			if (circle_number == 1)
				gameManger.GetComponent<Achievements> ().UnlockAchievement ("The NewB");
			// update clicked amount of circles
			PlayerPrefs.SetInt("Circlecount", (PlayerPrefs.GetInt("Circlecount") + circle_number));
			// set game over
			gameManger.GetComponent<GameLogic>().triggerGameOver();
		}
	}
}
