﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Achievements : MonoBehaviour {

	//int new_kid, fk_the_system, touch_the_rainbow, rage_quit, multi_fingering, fingering_spree, solo_fingertration, double_fingertration, early_bird, nightowl;
	List<string> achieved_achievements = new List<string>();
	List<string> possible_achievements = new List<string>();
	List<string> recent_unlocked_achievements = new List<string>();
	bool show_onetime = false;
	bool in_menu = false;
	int counter;

	public GameObject header;
	public GameObject main_text;
	public GameObject gameoverGO;
	public GameObject achievementGO;
	public GameObject menu;

	//Namen nicht ändern!
	public GameObject Bircircle;
	public GameObject WhatGoesAround;
	public GameObject ComesBackAround;
	public GameObject TheySeeMeRollin;
	public GameObject FingerLickingGood;
	public GameObject CheGuefinger;
	public GameObject Kreiselficker;
	public GameObject TheNewB;
	public GameObject FuckTheSystem;
	public GameObject MultiFingering;
	List<GameObject> achievements_name_go = new List<GameObject> ();

	void Start(){
		//PlayerPrefs.DeleteAll ();

		achievements_name_go.Add (Bircircle);
		achievements_name_go.Add (WhatGoesAround);
		achievements_name_go.Add (ComesBackAround);
		achievements_name_go.Add (TheySeeMeRollin);
		achievements_name_go.Add (FingerLickingGood);
		achievements_name_go.Add (CheGuefinger);
		achievements_name_go.Add (Kreiselficker);
		achievements_name_go.Add (TheNewB);
		achievements_name_go.Add (FuckTheSystem);
		achievements_name_go.Add (MultiFingering);

		//default value of GetInt is 0 if empty
		// ---- IMPLEMENTED ----
		possible_achievements.Add("Bicircle");
		possible_achievements.Add("What Goes Around");
		possible_achievements.Add("Comes Back Around");
		possible_achievements.Add("They See Me Rollin");
		possible_achievements.Add("Finger Licking Good");
		possible_achievements.Add("Che Guefinger");
		possible_achievements.Add("Kreiselficker");
		possible_achievements.Add("The NewB");
		possible_achievements.Add("Fuck The System");
		possible_achievements.Add("Multi Fingering");
		// ---- NOT IMPLEMENTED ----
//		possible_achievements.Add("Touch The Rainbow");
//		possible_achievements.Add("Rage Quit");
//		possible_achievements.Add("Fingering Spree");
//		possible_achievements.Add("Solo Fingertration");
//		possible_achievements.Add("Double Fingertration");
//		possible_achievements.Add("Early Bird");
//		possible_achievements.Add("Nightowl");
	}

	public void UnlockAchievement(string s){
		if (PlayerPrefs.GetInt(s) == 0) {
			PlayerPrefs.SetInt (s, 1);
			achieved_achievements.Add(s);
			recent_unlocked_achievements.Add(s);
			show_onetime = true;
		}
	}

	public List<GameObject> GetAchievementGos(){
		return achievements_name_go;
	}

	public void ToggleAchievement(){
		achieved_achievements.Clear ();
		counter = 0;
		//fill unlocked achievements list
		for (int i = 0; i < possible_achievements.Count; i++) {
			if (PlayerPrefs.GetInt (possible_achievements [i]) == 1) {
				counter ++;
				achieved_achievements.Add(possible_achievements [i]);
			}
		}

		achievementGO.SetActive (!achievementGO.activeSelf);
		if (achievementGO.activeSelf) {
			//Check which Achievements are unlocked and activate
			//Wichtig: Strings zwischen skript und GOs werde verglichen. Vorsicht!
			foreach (string s in achieved_achievements) {
				//remove whitespaces from strings
				s.Replace (" ", "");
				foreach (GameObject go in achievements_name_go) {
					if (go.name.Equals (s)) {
						go.SetActive (true);
					}
				}
			}
		} else {
			foreach (GameObject go in achievements_name_go) {
				go.SetActive (false);
			}
		}

		SetHeader ();
		if (menu.activeSelf) {
			menu.SetActive (false);
			in_menu = true;
		}else if (in_menu) {
			menu.SetActive (true);
			in_menu = false;
		}else {
			gameoverGO.SetActive (!gameoverGO.activeSelf);
		}

	}

	private void SetHeader(){
		header.GetComponent<Text> ().text = "Achievements: " + counter + "/" + possible_achievements.Count;
	}
	
	public List<string> GetUnlockedAchievements(){
		return achieved_achievements;
	}
	
	public List<string> GetPossibleAchievements(){
		return possible_achievements;
	}
	
	public List<string> GetRecentUnlockedAchievements(){
		return recent_unlocked_achievements;
	}

	public bool GetOnetime(){
		return show_onetime;
	}

	public void SetOneTimeAndCleanRecent(){
		show_onetime = false;
		recent_unlocked_achievements.Clear ();
	}
}
