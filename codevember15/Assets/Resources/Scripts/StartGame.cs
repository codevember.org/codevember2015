﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class StartGame : MonoBehaviour
{
    public GameObject canvas_extra_btns;
    public Button strtBtn;
    public GameObject menu;
    public GameObject game_manager;
    public GameObject score;
    public GameObject band;
    GameObject buttonAnimated;
    bool activated;

    void Start()
    {
        score.SetActive(false);
        buttonAnimated = GameObject.Find("StartButton");
        buttonAnimated.GetComponent<Animator>().enabled = false;
    }

    // animation
    IEnumerator WaitForAnimation()
    {
        GameObject repeater = GameObject.Find("repeator");

        if (!activated)
        {
            if (!repeater.GetComponent<repeatController>().new_round)
            {
                buttonAnimated.GetComponent<Animator>().enabled = true;
                // buttonAnimated.GetComponent<Animator>().SetTime(0.0f);
                buttonAnimated.GetComponent<Animator>().cullingMode = AnimatorCullingMode.AlwaysAnimate;
                buttonAnimated.GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(1);
            }

            // show score
            score.SetActive(true);
            // start game logic
            game_manager.GetComponent<GameLogic>().setRunning();
            // hide side buttons while game is running
            canvas_extra_btns.SetActive(false);
        }

        // disable complete start menu
        menu.SetActive(activated);
    }

    // starts the game if active = false
    public void Run(bool active)
    {
        strtBtn.interactable = false;
        activated = active;
        StartCoroutine(WaitForAnimation());
    }
}