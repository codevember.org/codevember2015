﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class Score : MonoBehaviour {

	// current score
    private int score;
	// best score which was reached
    private int bestScore;

	// Use this for initialization
	void Start () {
		score = 0;
        bestScore = PlayerPrefs.GetInt("Highscore");
		GameObject.Find("Score").GetComponent<Text>().text = "Score: " + score.ToString();
	}

    /**
    update score with 1 point according to game speed
    **/
    public void updateScore(int toAdd) {
        score += toAdd;
		GameObject.Find("Score").GetComponent<Text>().text = "Score: " + score.ToString();
    }

	// update best score
    public void onGameOver() {
		if (score > bestScore) {
			PlayerPrefs.SetInt ("Highscore", score);
			bestScore = score;
		}
    }

	// get current score
	public int getScore() {
		return this.score;
	}

	// get best score
	public int getBestScore() {
		return this.bestScore;
	}
}
