﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gameover : MonoBehaviour {
	public GameObject canvas_extra_btns;
	public GameObject gameOverBox;
	public GameObject endscore;
	public GameObject displayscore;
    public GameObject bestscore;
	public GameObject achievementPanel;
	Achievements achievement_comp;
	public GameObject gameoverGo;
	public Image achiev_image;

	public Image Bircircle;
	public Image WhatGoesAround;
	public Image ComesBackAround;
	public Image TheySeeMeRollin;
	public Image FingerLickingGood;
	public Image CheGuefinger;
	public Image Kreiselficker;
	public Image TheNewB;
	public Image FuckTheSystem;
	public Image MultiFingering;
	List<Image> achiev_images = new List<Image> ();

	public GameObject replayBtn;
	public GameObject tropyGold;
	public GameObject tropySilver;
	public GameObject tropyBronze;
	public GameObject tropyNon;
	public ParticleSystem particles;

	void Start(){
		achiev_images.Add (Bircircle);
		achiev_images.Add (WhatGoesAround);
		achiev_images.Add (ComesBackAround);
		achiev_images.Add (TheySeeMeRollin);
		achiev_images.Add (FingerLickingGood);
		achiev_images.Add (CheGuefinger);
		achiev_images.Add (Kreiselficker);
		achiev_images.Add (TheNewB);
		achiev_images.Add (FuckTheSystem);
		achiev_images.Add (MultiFingering);

		achievement_comp = gameObject.GetComponent<Achievements> ();
	}
		
	public IEnumerator startFirework(Transform t) {
		
		Vector3 newPos = new Vector3(0,0,0);
		// hard coded
		float width = 500;
		float height = 500;
		float x, y, r, g, b;

		yield return new WaitForSeconds(0.5f);
		//ParticleSystem ps = new ParticleSystem();

		for(int i = 0; i < 10; i++) {
			
			x = UnityEngine.Random.Range (-(width / 2), (width / 2));
			y = UnityEngine.Random.Range (-(height / 2), (height / 2));

			r = UnityEngine.Random.Range (0.1f, 1.0f);
			g = UnityEngine.Random.Range (0.1f, 1.0f);
			b = UnityEngine.Random.Range (0.1f, 1.0f);

			newPos = new Vector3 (x, y, -3.05f);

			//ps = Instantiate(particles);
			particles.transform.Translate(newPos, Space.World);
			particles.startColor = new Color(r, g, b);
			particles.Play();

			yield return new WaitForSeconds(0.5f);
			particles.Stop();
			particles.Clear();
		}

		yield return new WaitForSeconds(0.5f);
		//Destroy(ps);
	}

    IEnumerator WaitForMusic() {
        GameObject.Find("Circles").SetActive(false);
        gameObject.GetComponent<GameLogic>().unsetRunning();

		GameObject mus = GameObject.Find ("Particles");
		mus.GetComponent<AudioSource>().Play();
		// disable replay button as long score animation is running
		replayBtn.SetActive (false);
		// get current score and best score
		int score = gameObject.GetComponent<Score> ().getScore ();
		int bestScore = gameObject.GetComponent<Score> ().getBestScore ();
		// count up score and if necessary best score
		StartCoroutine(countToTargetScore(score, bestScore));
		// update best score
		gameObject.GetComponent<Score>().onGameOver();
		// wait few millisecs bevore displaying the game over overlay
		gameOverBox.SetActive(false);
		yield return new WaitForSeconds(0.8f);
		gameOverBox.SetActive(true);

		// start firework if highscore
		if(score > bestScore)
			StartCoroutine(startFirework(gameObject.transform));

		// set side buttons active
		canvas_extra_btns.SetActive (true);
		// display unlocked achievements
		if (achievement_comp.GetOnetime ()) {
			foreach (string s in achievement_comp.GetRecentUnlockedAchievements()){
				yield return new WaitForSeconds(1);
				achievementPanel.SetActive(true);
				achievementPanel.GetComponentInChildren<Text>().text = "Achievement Unlocked! "+ s;
				foreach (Image img in achiev_images){
					if (s.Equals(img.transform.parent.name)){
						achiev_image.overrideSprite = img.sprite;
					}
				}
				yield return new WaitForSeconds(3);
			}
			achievementPanel.SetActive(false);
			achievement_comp.SetOneTimeAndCleanRecent();
		}
        //Debug.Log ("GameOver!");
	}

	// count score to target score (little animation)
	IEnumerator countToTargetScore(int target, int bestScore) {
		int start = 0;
		float duration;
		if (target > 20)
			duration = 1.0f;
		else
			duration = 0.5f;
		float progress = 0.0f;
		// of currenct score is smaller than best score just animate the current score
		if (target <= bestScore) {
			bestscore.GetComponent<Text> ().text = gameObject.GetComponent<Score> ().getBestScore ().ToString();
			endscore.GetComponent<Text> ().text = start.ToString ();
			yield return new WaitForSeconds (1.0f);
			for (float timer = 0; timer < duration; timer += Time.deltaTime) {
				progress = timer / duration;
				endscore.GetComponent<Text> ().text = ((int)Mathf.Lerp (start, target, progress)).ToString ();
				yield return null;
			}
			endscore.GetComponent<Text> ().text = target.ToString ();
			// set trophy icon depending on reached score
			StartCoroutine(setTrophy (target));
			replayBtn.SetActive (true);
		} else {
			// if current score beats best score animate both
			int score = 0;
			endscore.GetComponent<Text> ().text = start.ToString ();
			bestscore.GetComponent<Text> ().text = start.ToString ();
			yield return new WaitForSeconds (1.0f);
			for (float timer = 0; timer < duration; timer += Time.deltaTime) {
				progress = timer / duration;
				score = (int)Mathf.Lerp (start, target, progress);
				endscore.GetComponent<Text> ().text = score.ToString ();
				bestscore.GetComponent<Text> ().text = score.ToString ();
				yield return null;
			}
			endscore.GetComponent<Text> ().text = target.ToString ();
			bestscore.GetComponent<Text> ().text = target.ToString ();
			// set trophy icon depending on reached score
			StartCoroutine(setTrophy (target));
			replayBtn.SetActive (true);
		}
	}

	// Display Trophy depending on reached score
	private IEnumerator setTrophy(int score) {
		float gold = 180;
		float silver = 120;
		float bronze = 60;
		//gold
		if (score >= gold) {
			tropyGold.SetActive (true);
			tropyBronze.SetActive (false);
			tropySilver.SetActive (false);
			yield return new WaitForSeconds (1.5f);
			tropyNon.SetActive (false);
		}
		//silber
		else if (score >= silver  && score < gold) {
			tropySilver.SetActive (true);
			tropyBronze.SetActive (false);
			tropyGold.SetActive (false);
			yield return new WaitForSeconds (1.5f);
			tropyNon.SetActive (false);
		}
		//bronze
		else if (score >= bronze && score < silver) {
			tropyBronze.SetActive (true);
			tropySilver.SetActive (false);
			tropyGold.SetActive (false);
			yield return new WaitForSeconds (1.5f);
			tropyNon.SetActive (false);
		} 
		yield return null;
	}

	public void PopupGameover () {
		gameoverGo.SetActive (true);
		StartCoroutine(WaitForMusic());
	}

	public void DestroyPopup() {
		// disable side buttons while new game will be started
		canvas_extra_btns.SetActive (false);
		GameObject repeater = GameObject.Find("repeator");
		repeater.GetComponent<repeatController>().new_round = true;
		// restart game
		SceneManager.LoadScene("superScene");
	}
}


