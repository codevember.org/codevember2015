﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleSound : MonoBehaviour {

	public Sprite sound_on;
	public Sprite sound_off;
	public Button btn_sound;

	private int sound;

	// Use this for initialization
	void Start () {
		// save audio settings for later gaming sessions
		sound = PlayerPrefs.GetInt ("Sound");
		if (sound == 1)
			AudioListener.pause = true;
		else if (sound == 0)
			AudioListener.pause = false;
			
		checkSprites ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void checkSprites() {
		// handle button sprite
		if (AudioListener.pause)
			btn_sound.GetComponent<Button> ().image.overrideSprite = sound_off;
		else
			btn_sound.GetComponent<Button> ().image.overrideSprite = sound_on;
	}

	public void toggleSoundAndIcon() {
		// handle button sprite
		if (!AudioListener.pause) {
			btn_sound.GetComponent<Button> ().image.overrideSprite = sound_off;
			PlayerPrefs.SetInt ("Sound", 1);
		} else {
			btn_sound.GetComponent<Button> ().image.overrideSprite = sound_on;
			PlayerPrefs.SetInt ("Sound", 0);
		}
		// update audio listener
		AudioListener.pause = !AudioListener.pause;
	}
}
